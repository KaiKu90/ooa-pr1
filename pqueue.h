//
// Created by Kai Kuhlmann on 07.04.16.
//

#ifndef OOA_PR1_PQUEUE_H
#define OOA_PR1_PQUEUE_H

typedef struct priorityqueue priorityqueue_t;

priorityqueue_t *pqueue_create();
void pqueue_insert(priorityqueue_t *pq, char *value, float prio);
char *pqueue_extractMin(priorityqueue_t *pq);
void pqueue_decreaseKey(priorityqueue_t *pq, char *value, float prio);
void pqueue_removeEntry(priorityqueue_t *pq, char *value);
void pqueue_destroyPriorityQueue(priorityqueue_t *pq);
void pqueue_sortQueue(priorityqueue_t *pq);
void pqueue_printQueue(priorityqueue_t *pq);
int pqueue_valueAlreadyExists(priorityqueue_t *pq, char *value);

#endif //OOA_PR1_PQUEUE_H
