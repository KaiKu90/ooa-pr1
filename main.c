//
// Created by Kai Kuhlmann on 07.04.16.
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "pqueue.h"

#define MAX 100000

char *randomString(int size) {
    char *string = (char *) malloc((size+1) * sizeof(char));

    for (int i = 0; i < size; i++) {
        string[i] = (rand() % 26) + 'A';
    }

    string[size] = '\0';
    return string;
}

int main() {
    priorityqueue_t *pq = NULL;
    char *val1[] = {"Test0", "Test1", "Test2", "Test3", "Test4", "Test5"};
    char *val2[] = {"Test6", "Test7", "Test8", "Test9"};
    float prio1[] = {15, 30, 10, 45, 5, 40};
    float prio2[] = {25, 20, 35, 50};
    char *strings[MAX];
    clock_t tic, toc;

    //create queue
    pq = pqueue_create();
    if (pq == NULL) {
        printf("\nERROR: Memory for queue could not be allocated\n");
        return EXIT_FAILURE;
    } else {
        printf("\nQueue successfully created\n\n");
    }

    //insert some entries
    for (int i = 0; i < 6; i++) {
        pqueue_insert(pq, val1[i], prio1[i]);
    }

    printf("Insert some entries to queue");
    pqueue_printQueue(pq);  //print the queue

    //insert some more entries
    for (int i = 0; i < 4; i++) {
        pqueue_insert(pq, val2[i], prio2[i]);
    }

    printf("Insert some more entries to queue");
    pqueue_printQueue(pq);  //print the queue

    //print the the value with the highest priority and deletes it from the queue
    printf("Extract value with highest priority in queue\n");
    printf("Value = %s", pqueue_extractMin(pq));
    pqueue_printQueue(pq);

    //decrease the priority of value "Test9" from 50 to 1
    printf("Decrease priority-value of value \"Test9\"");
    pqueue_decreaseKey(pq, val2[3], 1);
    pqueue_printQueue(pq);

    //remove entry with value "Test3"
    printf("Remove entry with value \"Test2\"");
    pqueue_removeEntry(pq, val1[2]);
    pqueue_printQueue(pq);

    //destroy the whole queue
    pqueue_destroyPriorityQueue(pq);
    printf("Queue successfully destroyed\n");


    srand(time(NULL));

    for (int i = 0 ; i < MAX ; ++i) {
        strings[i] = randomString(8);
    }

    //create queue
    pq = pqueue_create();
    if (pq == NULL) {
        printf("\nERROR: Memory for queue could not be allocated\n");
        return EXIT_FAILURE;
    } else {
        printf("\nQueue successfully created\n\n");
    }

    //time measuring for pqueue_insert()
    printf("Measuring time for function pqueue_insert() ...\n");
    tic = clock();
    for (int i = 0 ; i < MAX ; ++i) {
        pqueue_insert(pq, strings[i], rand() % 100);
    }
    toc = clock();

    printf("Insertion time: %.2f\n\n", (float) (toc-tic) / CLOCKS_PER_SEC);

    //time measuring for pqueue_extractMin()
    printf("Measuring time for function pqueue_extractMin() ...\n");
    tic = clock();
    for (int i = 0 ; i < MAX ; ++i) {
        pqueue_extractMin(pq);
    }
    toc = clock();

    printf("Extract time: %.2f\n\n", (float) (toc-tic) / CLOCKS_PER_SEC);

    //free memory
    for (int i = 0 ; i < MAX ; ++i) {
        free(strings[i]);
    }

    //destroy the whole queue
    pqueue_destroyPriorityQueue(pq);
    printf("Queue successfully destroyed\n");

    return EXIT_SUCCESS;
}