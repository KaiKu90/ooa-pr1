//
// Created by Kai Kuhlmann on 07.04.16.
//

#include <stdio.h>
#include <stdlib.h>
#include "pqueue.h"

typedef struct {
    char *value;
    float prio;
}pqentry_t;

struct priorityqueue {
    int size;
    int last;
    pqentry_t *pqentry;
};

priorityqueue_t *pqueue_create() {
    priorityqueue_t *pq = NULL;
    pq = malloc(sizeof(priorityqueue_t));

    if (pq == NULL) return NULL;

    pq->size = 0;

    return pq;
}

int pqueue_valueAlreadyExists(priorityqueue_t *pq, char *value) {
    for (int i = 0; i <= pq->last; i++) {
        if (pq->pqentry[i].value == value) return 1;
    }

    return 0;
}

void pqueue_insert(priorityqueue_t *pq, char *value, float prio) {
    if (pq->size == 0) {
        pq->pqentry = malloc(sizeof(pqentry_t));

        if (pq->pqentry == NULL) {
            printf("ERROR: Memory for \"pq->entry\" could not be allocated\n");
            return;
        }

        pq->pqentry[0].value = value;
        pq->pqentry[0].prio = prio;

        pq->size = 1;
        pq->last = 0;
    } else {
        //if (pqueue_valueAlreadyExists(pq, value)) return;

        if (pq->last == pq->size-1) {
            pq->size *= 2;
            pq->pqentry = realloc(pq->pqentry, pq->size * sizeof(pqentry_t));

            if (pq->pqentry == NULL) {
                printf("ERROR: Memory for \"pq->entry\" could not be reallocated\n");
                return;
            }
        }

        if (pq->pqentry[pq->last].prio > prio) {
            pq->pqentry[pq->last+1].prio = prio;
            pq->pqentry[pq->last+1].value = value;
        } else {
            for (int i = pq->last; i >= 0 && pq->pqentry[i].prio < prio; i--) {
                pq->pqentry[i+1] = pq->pqentry[i];

                if (pq->pqentry[i-1].prio > prio || i == 0) {
                    pq->pqentry[i].value = value;
                    pq->pqentry[i].prio = prio;
                }
            }
        }

        pq->last += 1;
    }
}

char *pqueue_extractMin(priorityqueue_t *pq) {
    char *minVal = pq->pqentry[pq->last].value;

    pq->last -= 1;

    return minVal;
}

void pqueue_decreaseKey(priorityqueue_t *pq, char *value, float prio) {
    for (int i = 0; i <= pq->last; i++) {
        if (pq->pqentry[i].value == value) {
            pq->pqentry[i].prio = prio;
        }
    }

    pqueue_sortQueue(pq);
}

void pqueue_removeEntry(priorityqueue_t *pq, char *value) {
    for (int i = 0; i <= pq->last; i++) {
        if (pq->pqentry[i].value == value) {
            for (int j = i; j <= pq->last; j++) {
                pq->pqentry[j] = pq->pqentry[j+1];
            }
        }
    }

    pq->last -= 1;
}

void pqueue_destroyPriorityQueue(priorityqueue_t *pq) {
    free(pq->pqentry);
    pq->pqentry = NULL;
    free(pq);
}

void pqueue_sortQueue(priorityqueue_t *pq) {
    pqentry_t pqe_tmp;

    for (int i = 0; i <= pq->last; i++) {
        int max = i;

        for (int j = i; j <= pq->last; j++) {
            if (pq->pqentry[j].prio > pq->pqentry[max].prio) {
                max = j;
            }
        }

        pqe_tmp = pq->pqentry[max];
        pq->pqentry[max] = pq->pqentry[i];
        pq->pqentry[i] = pqe_tmp;
    }
}

void pqueue_printQueue(priorityqueue_t *pq) {
    printf("\n");

    if (pq == NULL) {
        printf("Queue successfully destroyed\n");
        return;
    }

    for (int i = 0; i <= pq->last; i++) {
        printf("pq[%d] | value = %s | priority = %f\n", i, pq->pqentry[i].value, pq->pqentry[i].prio);
    }

    printf("\n");
}